package uz.com.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import uz.com.entity.Employee;
import uz.com.repository.EmployeeRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@Transactional
public class EmployeeService {

    @Autowired
    EmployeeRepository repository;
    public List<Employee> listAllEmployee(){
        return repository.findAllEmployeeWithoutAdmin();
    }
    public Optional<Employee>getById(UUID id){
        Optional<Employee> employee =repository.findById(id);
        return employee;
    }
}
