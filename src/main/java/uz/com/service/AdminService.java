package uz.com.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import uz.com.entity.Drug;
import uz.com.repository.DrugRepository;

@Service
@Transactional
public class AdminService {
    @Autowired
    DrugRepository drugRepository;

    public void saveDrug(Drug drug) {
        drugRepository.save(drug);
    }
}
