package uz.com.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.com.entity.Drug;
@Repository
public interface DrugRepository extends JpaRepository<Drug, Integer> {
}
