package uz.com.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import uz.com.entity.Employee;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, UUID> {
    Optional<Employee> findByUsername(String username);

    @Query(value = "select * from public.employee\n" +
            "where employee.id not in (select employee.id from\n" +
            "public.employee where employee.id=\n" +
            "(select user_id from public.user_role where user_role.role_id=10)\n" +
            "or employee.account_non_locked='false')", nativeQuery = true)
    List<Employee> findAllEmployeeWithoutAdmin();

    @Query(value = "select * from public.employee\n" +
            "where employee.account_non_locked='false'", nativeQuery = true)
    List<Employee> findDeletedAllEmployee();
}
