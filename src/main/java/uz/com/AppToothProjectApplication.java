package uz.com;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppToothProjectApplication {

    public static void main(String[] args) {
        SpringApplication.run(AppToothProjectApplication.class, args);
    }

}
