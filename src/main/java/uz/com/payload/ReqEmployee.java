package uz.com.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.*;
import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class ReqEmployee {

    @NotBlank(message = "First name required!")
    private String firstName;
    @NotBlank(message = "Last name required!")
    private String lastName;
    private String username;
    private String password;
    @NotNull(message = "Age required!")
    @Min(value = 18, message = "Age must be over 18!")
    private int age;
    @NotBlank(message = "Email required!")
    @Email(regexp = ".+@.+\\..+", message = "Email must include @ !")
    private String email;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @NotNull(message = "birthday required!")
    @Past(message = "Birthday must be in past time!")
    private Date birthday;
    @NotBlank(message = "Phone required!")
    @Pattern(regexp = "^\\+9989\\d{8}",message = "Enter only UZB number")
    private String phone;
    @NotBlank(message = "Position required!")
    private String position;
    @NotBlank(message = "Office required!")
    private String office;
    @NotBlank(message = "Address required!")
    private String address;
}
