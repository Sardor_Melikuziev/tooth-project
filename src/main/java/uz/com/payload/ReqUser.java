package uz.com.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;


@NoArgsConstructor
@AllArgsConstructor
@Data
public class ReqUser {
    @NotBlank(message = "First Name required!!!")
    private String firstName;
    @NotBlank(message = "Last Name required!!!")
    private String lastName;
    @NotBlank(message = "Phone required!")
    @Pattern(regexp = "^\\+9989\\d{8}",message = "Enter only UZB number")
    private String phone;
    @NotBlank(message = "Description required!!!")
    private String description;
}
