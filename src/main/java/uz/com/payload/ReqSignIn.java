package uz.com.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class ReqSignIn {

    @NotBlank(message = "User name should not be null")
    private String username;

    private String password;
}
