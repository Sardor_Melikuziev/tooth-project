package uz.com.loader;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import uz.com.entity.Employee;
import uz.com.entity.RoleName;
import uz.com.repository.EmployeeRepository;
import uz.com.repository.RoleRepository;

import java.text.SimpleDateFormat;


@Component
public class DataLoader implements CommandLineRunner {
    SimpleDateFormat simpleDateFormat=new SimpleDateFormat("mm/DD/yyyy");
    @Autowired
    EmployeeRepository employeeRepository;
    @Autowired
    PasswordEncoder encoder;
    @Autowired
    RoleRepository roleRepository;
    @Value("${spring.datasource.initialization-mode}")
    private String initMode;

    @Override
    public void run(String... args) throws Exception {
        if (initMode.equals("always")) {
            employeeRepository.save(new Employee(
                    "Sardorbek",
                    "Melikuziev",
                    "admin",
                    encoder.encode("root123"),
                    22,
                    "sardormelikuziev707@gmail.com",
                    simpleDateFormat.parse("11/01/1996"),
                    "+998 93 632 23 45",
                    "Andijon region balikchi city Gulistan MFY",
                    roleRepository.findAll()
            ));
            employeeRepository.save(new Employee(
                    "Ravshan",
                    "Soatov",
                    "roma",
                    encoder.encode("rom007"),
                    22,
                    "ravshan@mail.ru",
                    simpleDateFormat.parse("01/01/1997"),
                    "+998 99 123 45 67",
                    "Andijon balikchi turkistan",
                    roleRepository.findAllByRole(RoleName.ROLE_MANGER)
            ));
        }
    }
}
