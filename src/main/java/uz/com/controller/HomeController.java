package uz.com.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class HomeController {
    @GetMapping("/")
    public String goHome(){
        return "home";
    }
    @GetMapping("/login")
    public String goLogin(@RequestParam(defaultValue = "false") boolean error,
                          Model model) {
        model.addAttribute("error", error);
        return "login";
    }
    @GetMapping("/cabinet")
    public String employeeCabinet(){
        return "cabinet";
    }

}
