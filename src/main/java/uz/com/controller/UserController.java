package uz.com.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import uz.com.payload.ReqUser;
import uz.com.repository.DrugRepository;
import uz.com.repository.UserRepository;

import javax.validation.Valid;

@Controller
@RequestMapping("/user")
public class UserController {
    @Autowired
    UserRepository userRepository;
    @Autowired
    DrugRepository drugRepository;

    @GetMapping("/view")
    public String userView(ReqUser reqUser) {
        return "addUser";
    }

    @PostMapping("/add")
    public String addUser(@Valid ReqUser reqUser, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            return "addUser";
        } else {
            model.addAttribute("drugs",drugRepository.findAll());
            model.addAttribute("user", reqUser);
            return "userDrugs";
        }
    }
}
