package uz.com.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import uz.com.entity.Drug;
import uz.com.entity.Employee;
import uz.com.repository.DrugRepository;
import uz.com.repository.EmployeeRepository;
import uz.com.repository.RoleRepository;
import uz.com.service.AdminService;
import uz.com.service.EmployeeService;

import java.util.List;
import java.util.UUID;

@Controller
@RequestMapping("/admin")
public class AdminController {
    @Autowired
    EmployeeService service;
    @Autowired
    EmployeeRepository employeeRepository;
    @Autowired
    RoleRepository roleRepository;
    @Autowired
    DrugRepository drugRepository;
    @Autowired
    AdminService adminService;

    @GetMapping("/deleted/employee/list")
    public String deletedEmployee(Model model) {
        List<Employee> employees = employeeRepository.findDeletedAllEmployee();
        model.addAttribute("employees", employees);
        return "deletedEmployee";
    }

    @GetMapping("/employee/retake")
    public String retakeEmployee(@RequestParam("id") UUID id) {
        Employee employee = service.getById(id).get();
        employee.setAccountNonLocked(true);
        employeeRepository.save(employee);
        return "redirect:/admin/deleted/employee/list";
    }

    @GetMapping("/add/drug")
    public String addDrug(Model model) {
        List<Drug> drugs = drugRepository.findAll();
        model.addAttribute("drugs", drugs);
        return "drugList";
    }

    @PostMapping("/save/drug")
    public String saveDrug(@ModelAttribute Drug drug) {
        adminService.saveDrug(drug);
        return "redirect:/admin/add/drug";
    }

    @GetMapping("/delete/drug")
    public String deleteDrug(@RequestParam("id") Integer id) {
        drugRepository.deleteById(id);
        return "redirect:/admin/add/drug";
    }

    @GetMapping("/find/drug")
    @ResponseBody
    public Drug findDrug(@RequestParam("id") Integer id) {
        return drugRepository.findById(id).get();
    }
}
