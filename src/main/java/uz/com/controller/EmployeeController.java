package uz.com.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import uz.com.entity.Employee;
import uz.com.entity.RoleName;
import uz.com.payload.ReqEmployee;
import uz.com.repository.EmployeeRepository;
import uz.com.repository.RoleRepository;
import uz.com.service.EmployeeService;

import javax.validation.Valid;
import java.util.Optional;
import java.util.UUID;

@Controller
@RequestMapping("/employee")
public class EmployeeController {

    @Autowired
    EmployeeRepository employeeRepository;
    @Autowired
    PasswordEncoder encoder;
    @Autowired
    RoleRepository roleRepository;
    @Autowired
    AuthenticationManager authenticationManager;
    @Autowired
    EmployeeService service;

    @GetMapping("/cabinet")
    public String employeeCabinet() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return "cabinet";
    }

    @GetMapping("/register/view")
    public String registerView(ReqEmployee reqEmployee, Model model) {
        model.addAttribute("roles", roleRepository.findAll());
        return "register";
    }

    @PostMapping("/add")
    public String employeeAdd(@Valid ReqEmployee reqEmployee, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            return "register";
        } else {
            model.addAttribute("employee", reqEmployee);
            return "password";
        }
    }

    @PostMapping("/save")
    public String employeeSave(ReqEmployee reqEmployee, @RequestParam(defaultValue = "false") boolean error, Model model) {
        Optional<Employee> byUsername = employeeRepository.findByUsername(reqEmployee.getUsername());
        if (byUsername.isPresent()) {
            model.addAttribute("error", error);
            model.addAttribute("employee", reqEmployee);
            return "password";
        } else {
            employeeRepository.save(new Employee(
                    reqEmployee.getFirstName(),
                    reqEmployee.getLastName(),
                    reqEmployee.getUsername(),
                    encoder.encode(reqEmployee.getPassword()),
                    reqEmployee.getAge(),
                    reqEmployee.getEmail(),
                    reqEmployee.getBirthday(),
                    reqEmployee.getPhone(),
                    reqEmployee.getPosition(),
                    reqEmployee.getAddress(),
                    reqEmployee.getOffice(),
                    roleRepository.findAllByRole(RoleName.ROLE_DOCTOR)
            ));
        }
        return "redirect:/?msg=success";
    }

    @GetMapping("/list")
    public String employeeList(Model model, @RequestParam(value = "msg", required = false) String message) {
        model.addAttribute("employees", service.listAllEmployee());
        model.addAttribute("msg", message);
        return "employeeList";
    }

    @GetMapping("/delete")
    public String deleteEmployee(@RequestParam("id") UUID id) {
        Employee employee = service.getById(id).get();
        employee.setAccountNonLocked(false);
        employeeRepository.save(employee);
        return "redirect:/employee/list";
    }

    @PostMapping("/edit")
    public String employeeEdit(Model model, @RequestParam("id") UUID id) {
        Employee employee = service.getById(id).get();
        model.addAttribute("employee", employee);
        return "editRegister";
    }


    @PostMapping("/update")
    public String employeeUpdate(Employee employee) {
        employeeRepository.save(employee);
        return "redirect:/employee/list";
    }


    @GetMapping("/findOne")
    @ResponseBody
    public Optional<Employee> findOne(@RequestParam("id") UUID id) {
        return service.getById(id);
    }
}
