package uz.com.entity;

public enum RoleName {
    ROLE_ADMIN,
    ROLE_MANGER,
    ROLE_USER,
    ROLE_DOCTOR,
    ROLE_COUNTER,
    ROLE_RECEPTION
}
