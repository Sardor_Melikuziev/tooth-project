package uz.com.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity(name = "drug")
public class Drug {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;
    private String code;
    private float cost;
    private String type;
    private String description;

    public Drug(String name, String code, float cost, String type, String description) {
        this.name = name;
        this.code = code;
        this.cost = cost;
        this.type = type;
        this.description = description;
    }

    @Override
    public String toString() {
        return "Drug{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", code='" + code + '\'' +
                ", cost=" + cost +
                ", type='" + type + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
