/**
 *
 */
$(document).ready(function () {
    $('.table .eBtn').on('click', function (event) {
        event.preventDefault();
        var href = $(this).attr('href');
        $.get(href, function (employee, status) {
            $('.myForm #id').val(employee.id);
            $('.myForm #firstName').val(employee.firstName);
            $('.myForm #lastName').val(employee.lastName);
            $('.myForm #username').val(employee.username);
            $('.myForm #password').val(employee.password);
            $('.myForm #age').val(employee.age);
            $('.myForm #email').val(employee.email);
            $('.myForm #birthday').val(employee.birthday);
            $('.myForm #phone').val(employee.phone);
            $('.myForm #position').val(employee.position);
            $('.myForm #office').val(employee.office);
            $('.myForm #address').val(employee.address);
        });
        $('.myForm #myModal').modal();
    });
    $('.nBtn, .table .dBtn').on('click', function (drugs) {
        drugs.preventDefault();
        var href = $(this).attr('href');
        var text = $(this).text();
        if (text == 'Edit') {
            $.get(href, function (drug, status) {
                $('.drugForm #id').val(drug.id);
                $('.drugForm #name').val(drug.name);
                $('.drugForm #code').val(drug.code);
                $('.drugForm #cost').val(drug.cost);
                $('.drugForm #type').val(drug.type);
                $('.drugForm #description').val(drug.description);
            });
            $('.drugForm #exampleModal').modal();
        }else {
            $('.drugForm #id').val('');
            $('.drugForm #name').val('');
            $('.drugForm #code').val('');
            $('.drugForm #cost').val('');
            $('.drugForm #type').val('');
            $('.drugForm #description').val('');
            $('.drugForm #exampleModal').modal();
        }
    });
});

